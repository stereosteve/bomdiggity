var assert = require('assert')
var calc = require('../app/calc')
var _ = require('lodash')

var fixtures = {
  bom: require('../public/data/bom2.json'),
  match: require('../public/data/match.json')
}

it('boom', function () {

  var bom__ = fixtures.bom

  var settings__ = {
    optimizer: 'overall'
    // multiplier
    // selectedSeller
    // optimizer
    // currency
  }

  var matches__ = {}
  _.each(fixtures.match.results, function (r) {
    matches__[r.reference] = r.items
  })

  // copes for deep equal check
  var bom___ = _.cloneDeep(bom__)
  var matches___ = _.cloneDeep(matches__)

  var bom = calc.calcBom(bom__, settings__, matches__)

  // assert bom computed
  assert.ok(bom.totalRows > 0)
  assert.equal(bom.coveredRows, 4)
  assert.ok(bom.perUnitPrice > 0, "Has per unit price")
  assert.ok(bom.grandTotal > 0, "Has grand total")
  assert.equal(bom.grandTotal, bom.perUnitPrice * bom.multiplier)

  // deep equal check
  assert.deepEqual(bom__, bom___)
  assert.deepEqual(matches__, matches___)

  // assert we get the same answer twice!
  var bom2 = calc.calcBom(bom__, settings__, matches__)
  assert.deepEqual(bom, bom2)

  console.log(bom)
  console.log(bom.availCurrencies)
})
