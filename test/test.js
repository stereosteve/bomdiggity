var assert = require('assert')
var _ = require('lodash')
var search = require('../public/data/search.json')
var parts = _.pluck(search.results, 'item')
var oj = require('../app/oj')

it('works', function () {
  assert.equal(search.results.length, 10)
})

it.skip('partPriceTable', function () {
  parts.forEach(function (part) {
    var computed = oj.partComputePrices(part, 'USD', 1)
    console.log("\n\n")
    console.log(part.offers)
    console.log(computed)
  })
})

it.skip('partsUnionSellers', function () {
  var sellers = oj.partsUnionSellers(parts)
  console.log(sellers)
})
