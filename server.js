var express = require('express')
var http = require('httpplease')
var _ = require('lodash')
var url = require('url')

var app = express()
app.use(express.static(__dirname + '/public'))

//
// match - calls to octopart api
//
app.get('/match', function(req, res, next) {
  var mpns = req.query.mpn
  if (!mpns) return res.status(400).send("mpn parameter is required")
  if (typeof mpns === 'string') mpns = [mpns]

  var queries = _.map(mpns, function (mpn) {
    return {
      mpn_or_sku: mpn,
      reference: mpn,
      limit: 20
    }
  })

  var params = [
    'queries=' + JSON.stringify(queries),
    'apikey=00e73295'
  ].join('&')

  var url = 'http://octopart.com/api/v3/parts/match?' + params
  console.log(url)

  http.get(url, function(err, r) {
    if (err) {
      console.error("ERR", err.text)
      return next(err);
    }
    var results = JSON.parse(r.text).results

    var matchMap = {}
    _.each(results, function (r) {
      matchMap[r.reference] = r.items
    })

    res.json(matchMap)
  })

})


//
// Suggest
//
app.get('/suggest', function (req, res, next) {
  http.get('http://octopart.com' + req.url, function(err, r) {
    if (err) return next(err)
    res.json(r.text.split("\n"))
  })
})

var port = process.env.PORT || 3002
app.listen(port)
console.log(':' + port)
