# bomdiggity

Experiment rewriting [bomtool](https://octopart.com/bom-lookup/manage) with React.

## Install

```
npm install
npm start      # starts webpack with --watch to recomplie with changes
node server    # in a separate terminal
```

Visit: localhost:3002


## Notes


BOM: just rows
  query
  qty

WORKSPACE: display and calc settings
  multiplier
  selectedSellers
  optimizer
  (currency)
  (country)



Bom actions:

```
= Add Line
= Remove Line
= Rename BOM
= Set Selected Distributors
= Change Bom Qty
= Change Pricing Optimization Model
= Compute total pricings
* (Export CSV)
* (Reorder Lines)
```

Line Actions

```
= Update Query
= Update Quantity
= Choose Match Item
```



## TODO

* OfferCell should show MOQ in red if below MOQ
* OfferCell should show stock in red if no stock or if quantity is above stock
* Standardize naming (lineitem or row but not both)
