module.exports = {
  cache: true,
  entry: './app/index',
  output: {
    filename: 'public/build/build.js'
  },
  module: {
    loaders: [
      {test: /\.js$/, loader: 'jsx-loader?harmony'}
    ]
  }
};
