var http = require('httpplease')

var constants = require('./constants')

exports.load = function(cb) {
  http.get('/data/bom2.json', function(err, res) {
    if (err) return cb(err)
    var bom = JSON.parse(res.text)
    return cb(null, bom)
  })
}





// Call /parts/match endpoint
// with an array of MPNs
exports.match = function(mpns, cb) {
  if (typeof mpns === 'string') {
    mpns = [mpns]
  }

  var params = _.map(mpns, function (mpn) {
    return 'mpn=' + encodeURIComponent(mpn)
  }).join('&')

  var url = '/match?' + params

  http.get(url, function(err, res) {
    if (err) return cb(err);
    var results = JSON.parse(res.text)
    cb(null, results)
  })
}
