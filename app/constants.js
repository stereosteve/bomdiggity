var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({
  LOADING_STATE: null,
  OK_STATE: null,
  ERROR_STATE: null
});
