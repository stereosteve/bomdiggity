var events = require('events')
var _ = require('lodash')
var BomClient = require('./bom_client')
var constants = require('./constants')
var oj = require('./oj')
var calc = require('./calc')
var uid = require('uid')

function BomStore() {
  this.bom = {
    name: 'Untitled BOM',
    rows: {}
  }

  this.settings = {
    optimizer: 'overall',
    sellerSelected: {
      'Digi-Key': true,
      'Newark': true,
      'Mouser': true
    }
  }

  this.matchDb = {}
  this.emitter = new events.EventEmitter()
}


// Register an event listener
BomStore.prototype.listen = function(fn) {
  this.emitter.addListener('change', fn)
}

// Deregister an event listener
BomStore.prototype.unlisten = function(fn) {
  this.emitter.removeListener('change', fn)
}

BomStore.prototype.changed = function() {
  this.emitter.emit('change')
}



// Get state - computes the bom and passes to view
BomStore.prototype.getState = function() {
  var bom = calc.calcBom(this.bom, this.settings, this.matchDb)
  return bom
}



// Load
BomStore.prototype.load = function () {
  var self = this
  BomClient.load(function(err, bom) {
    if (err) {
      // TODO: set error state, emit error event
      console.error('BomStore load failed', err)
      return
    }
    self.bom = bom
    self.changed()

    // now load the mpns
    // TODO: move this to a match store
    var mpns = _.pluck(bom.rows, 'query')
    BomClient.match(mpns, function (err, db) {
      if (err) throw err
      self.matchDb = db
      self.changed()
    })
  })
}

// setName
BomStore.prototype.setName = function(name) {
  this.bom.name = name
  this.emitter.emit('change')
}

BomStore.prototype.setBomMultiplier = function (num) {
  this.bom.multiplier = num
  this.emitter.emit('change')
}

// setLineQty
BomStore.prototype.setLineQty = function(id, qty) {
  var line = this.bom.rows[id]
  line.quantity = qty
  this.changed()
}

// setLineQuery
BomStore.prototype.setLineQuery = function(id, mpn) {
  var row = this.bom.rows[id]
  row.query = mpn
  row.state = constants.LOADING_STATE
  this.changed()

  var self = this
  BomClient.match(mpn, function (err, matchMap) {
    if (err) {
      console.error('addLineItemError', err)
      row.state = constants.ERROR_STATE
      return
    }
    _.extend(self.matchDb, matchMap)
    row.state = constants.OK_STATE
    self.changed()
  })
}

// setLineQuery
BomStore.prototype.setLineUid = function (lineId, uid) {
  var line = this.bom.rows[lineId]
  line.uid = uid
  this.changed()
}

// deleteRow
BomStore.prototype.deleteRow = function (rowId) {
  delete this.bom.rows[rowId]
  this.changed()
}

// addLineItem
BomStore.prototype.addLineItem = function(mpn) {

  var row = {
    id: uid(12),
    query: mpn,
    quantity: 1,
    state: constants.LOADING_STATE
  }

  this.bom.rows[row.id] = row
  this.changed()
  var self = this

  BomClient.match(mpn, function (err, matchMap) {
    if (err) {
      console.error('addLineItemError', err)
      row.state = constants.ERROR_STATE
      return
    }
    _.extend(self.matchDb, matchMap)
    row.state = constants.OK_STATE
    self.changed()
  })

}

// toggleSeller
BomStore.prototype.toggleSeller = function (seller) {
  this.settings.sellerSelected[seller] = !this.settings.sellerSelected[seller]
  this.changed()
}

// set optimizer
BomStore.prototype.setOptimizer = function (opt) {
  this.settings.optimizer = opt
  this.changed()
}

// set currency
BomStore.prototype.setCurrency = function (curr) {
  this.bom.currency = curr
  this.changed()
}

module.exports = new BomStore()
