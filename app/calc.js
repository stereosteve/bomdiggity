"use strict"

var _ = require('lodash')
var oj = require('./oj')



//
// Variables that start with `$` are API response objects
// They are inputs and references to external objects so they should not be modified
// Instead they are used to compute a presentation object to hand off to the view,
//   or cloned for further modification.
//
//    don't change $matches
//    don't change $part
//    don't change $offer
//


// TODO: compute seller info on BOM
// TODO: pass in more state (settings): selectedSellers, optimizationModel
// Finally, enable currency preference, country preference

/*
    calcBom
*/
function calcBom($bom, $settings, $matches) {
  var bom = _.cloneDeep($bom)
  bom.settings = _.cloneDeep($settings)

  bom.totalRows = _.keys(bom.rows).length
  bom.coveredRows = 0
  bom.perUnitPrice = 0
  bom.grandTotal = 0
  bom.sellerData = {}
  bom.availCurrencies = {}

  // some bom defaults
  bom.currency = bom.currency || 'USD'

  // calculate the rows
  _.each(bom.rows, function(r) {
    var $match = $matches[r.query]
    if (!$match) return
    calcRow(bom, r, $match)
  })

  // After rows are calculated - tally up some numbers!
  _.each(bom.rows, function(r) {
    if (isFinite(r.price)) {
      bom.coveredRows++
      bom.perUnitPrice += r.extendedTotal
    }

    // and some seller data
    if (!r.offers) return
    r.offers.forEach(function(o) {
      var seller = o.seller
      var sellerInfo = bom.sellerData[seller] = bom.sellerData[seller] || {
        rowsListed: 0,
        rowsStocked: 0,
        rowsAuthorized: 0,
        rowIds: {}
      }

      if (!sellerInfo.rowIds[r.id]) {
        sellerInfo.rowIds[r.id] = true
        sellerInfo.rowsListed++
        if (o.isStocked) sellerInfo.rowsStocked++
        if (o.authorized) sellerInfo.rowsAuthorized++
      }
    })

  })

  bom.grandTotal = bom.perUnitPrice * bom.multiplier
  bom.coverage = bom.coveredRows / bom.totalRows * 100

  return bom;

}


/**
    calcRow
*/
function calcRow(bom, row, $matches) {

  row.totalQuantity = row.quantity * bom.multiplier

  row.matches = {}
  _.each($matches, function($p) {
    var p = calcPart(bom, row, $p)
    p.totalQuantity = row.quantity * bom.multiplier
    p.avail = oj.partAvailAtQuantity($p, bom.currency, p.totalQuantity)
    row.matches[p.uid] = p
  })

  var $part = rowGetPart(row, $matches)
  if (!$part) return

  // process part, offers
  var part = row.part = calcPart(bom, row, $part)
  var offers = row.offers = calcPartOffers(bom, row, $part)

  // map seller name => offer
  row.sellerOffer = {}
  offers.forEach(function(o) {
    var seller = o.seller
    var other = row.sellerOffer[seller]
    if (
      !other
      || (o.price && !other.price)
      || (o.price < other.price)
    ) {
      row.sellerOffer[seller] = o
    }
  })

  // find best offer
  var ranked = _.chain(offers)
    .filter(function(o) {
      if (!o.price) return false;
      switch (bom.settings.optimizer) {
        case 'overall':
          return true;
        case 'selected':
          return bom.settings.sellerSelected[o.seller]
        default:
          return o.seller == bom.settings.optimizer
      }
    })
    .sortBy(function(o) {
      return (o.price)
    })
    .value()

  var bestOffer = ranked[0]
  if (bestOffer) {
    row.winner = bestOffer
    row.price = bestOffer.price
    row.extendedTotal = row.price * row.quantity
    row.grandTotal = row.extendedTotal * bom.multiplier
  }


}


/**
    calcPart
*/
function calcPart(bom, row, $part) {
  return {
    uid: $part.uid,
    brand: $part.brand.name,
    url: $part.octopart_url,
    mpn: $part.mpn,
    totalQuantity: row.totalQuantity, // TODO: remove this duplicate info
    avail: oj.partAvailAtQuantity($part, bom.currency, row.totalQuantity)
  }
}


function calcPartOffers(bom, row, $part) {
  return _.map($part.offers, function($offer) {
    return calcOffer(bom, row, $part, $offer)
  })
}


/**
    calcOffer
*/
function calcOffer(bom, row, $part, $offer) {

  var totalQuantity = row.quantity * bom.multiplier
  var price = oj.offerPriceAtQuantity($offer, bom.currency, totalQuantity)

  // prepare representation for view
  var o = {
    stock: $offer.in_stock_quantity,
    authorized: $offer.is_authorized,
    url: $offer.product_url,
    moq: $offer.moq,
    sku: $offer.sku,
    seller: $offer.seller.name,
    totalQuantity: totalQuantity,
    price: price,
    isStocked: (totalQuantity < $offer.in_stock_quantity),
    isAboveMoq: ($offer.moq && totalQuantity > $offer.moq)
  }

  return o

}


module.exports = {
  calcBom: calcBom,
  calcRow: calcRow,
  calcOffer: calcOffer
}










function rowGetPart(row, $matches) {

  // TODO: should use a global MatchStore
  var $part = $matches[0]

  // if user has selected a match (stored in row.uid) use it
  // otherwise use the first match
  if (row.uid) {
    $part = _.findWhere($matches, {uid: row.uid})
    if (!$part) throw new Error("Invalid UID: " + row.uid)
  }

  return $part
}
