/** @jsx React.DOM */

var React = require('react')
var Autocomplete = require('react-autocomplete')
var http = require('httpplease')


var MpnSuggest = React.createClass({

  render: function() {
    return this.transferPropsTo(
      <Autocomplete search={this._searchRemote} />
    )
  },

  _searchRemote: function(options, searchTerm, cb) {
    if (!searchTerm)  return cb(null, [])

    http.get('/suggest?q=' + searchTerm, onResult)

    function onResult(err, r) {
      if (err) return cb(err)
      var data = JSON.parse(r.text)

      data = data.map(d => (
        {id: d, title: d}
      ))

      // The above uses ES6's fat arrow syntax,
      // made possible by jsx-loader ?harmony flag
      //
      // It is equivalent to:
      //
      // data = data.map(function(d) {
      //   return {id: d, title: d}
      // })

      cb(null, data)
    }

  }

})



module.exports = MpnSuggest;
