/** @jsx React.DOM */

var React = require('react')
var BS = require('react-bootstrap')
var _ = require('lodash')
var BomStore = require('../bom_store')
var BomRow = require('./BomRow')
var SellerSelectorModal = require('./SellerSelectorModal')

//
// BomTable
//
var BomTable = React.createClass({

  render: function () {
    var bom = this.props.bom

    // TODO: precalculate and DRY this up
    var sellers = []
    _.each(bom.settings.sellerSelected, function(isShown, seller) {
      if (isShown) sellers.push(seller)
    })

    var trs = _.map(bom.rows, function(row) {
      return <BomRow key={row.id} bom={bom} row={row} sellers={sellers}/>
    })

    return (
      <table className="BomTable table table-striped">
        <thead>
          <tr>
            <th className="superHeader"></th>
            <th className="superHeader">Matched Parts</th>
            <th className="superHeader" colSpan={sellers.length}>
              <div className="pull-right">
                <BS.ModalTrigger modal={<SellerSelectorModal bom={bom}/>}>
                  <BS.Button bsSize="xsmall">Select</BS.Button>
                </BS.ModalTrigger>
              </div>
              Selected Distributors
            </th>
            <th className="superHeader" colSpan="4">
              <OptimizerSelector bom={bom} />
            </th>
          </tr>
          <tr>
            <th>Input</th>
            <th className="borderLeft borderRight">Part</th>
            {_.map(sellers, function(s) {
              return <th key={s} className="text-right">{s}</th>
            })}
            <th className="borderLeft">Distributor / SKU</th>
            <th className="text-right">Price</th>
            <th className="text-right">Extended Total</th>
            <th className="text-right">Grand Total</th>
          </tr>
        </thead>
        <tbody>
          {trs}
        </tbody>
      </table>
    )
  }
})


var OptimizerSelector = React.createClass({

  render: function () {
    var bom = this.props.bom
    var val = bom.settings.optimizer

    // TODO: precalculate and DRY this up
    var sellers = []
    _.each(bom.settings.sellerSelected, function(isShown, seller) {
      if (isShown) sellers.push(seller)
    })

    return <select value={val} onChange={this.setOptimizer}>
      <option value="overall">Lowest Price (overall)</option>
      <option value="selected">Lowest Price (selected)</option>
      <option disabled role="separator">----------</option>
      {sellers.map(function(s) {
        return <option value={s}>{s}</option>
      })}
    </select>
  },

  setOptimizer: function (e) {
    var val = e.target.value
    BomStore.setOptimizer(val)
  }
})


module.exports = BomTable
