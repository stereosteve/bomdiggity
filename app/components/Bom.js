/** @jsx React.DOM */
var React = require('react')
var _ = require('lodash')
var BomStore = require('../bom_store')
var BS = require('react-bootstrap')

var BomTable = require('./BomTable')
var MpnSuggest = require('./MpnSuggest')


//
// Bom
//
var Bom = React.createClass({

  getInitialState: function () {
    return {
      bom: BomStore.getState()
    }
  },

  componentDidMount: function() {
    BomStore.listen(this._onChange);
  },

  componentWillUnmount: function() {
    BomStore.unlisten(this._onChange);
  },

  _onChange: function() {
    this.setState({
      bom: BomStore.getState()
    })
  },

  render: function () {
    var bom = this.state.bom
    if (!bom) return null;

    return <div>

      <div className="pull-right text-right">
        <div>Per Item: {bom.perUnitPrice.toFixed(5)}</div>
        <div>BOM Total: {bom.grandTotal.toFixed(5)}</div>
        <div>BOM Coverage: {bom.coverage}%</div>
      </div>

      <div className="form-inline">
        <input className="form-control" type="text" value={bom.name} onChange={this.setBomName} />
        <input className="form-control" type="number" value={bom.multiplier} onChange={this.setBomMultiplier} />
        <select className="form-control" onChange={this.setCurrency}>
          {_.keys(bom.availCurrencies).map(function(curr) {
            return <option key={curr} value={curr}>{curr}</option>
          })}
        </select>
      </div>


      <br/>
      <BomTable bom={bom} />
      <hr/>

      <div className="form-inline">
        <MpnSuggest
          placeholder="Add MPN or SKU"
          onChange={this.addLineItem}
          />
      </div>
    </div>
  },

  setBomName: function (event) {
    BomStore.setName(event.target.value)
  },

  setBomMultiplier: function (event) {
    BomStore.setBomMultiplier(event.target.value)
  },

  setCurrency: function(event) {
    BomStore.setCurrency(event.target.value)
  },

  addLineItem: function(match) {
    BomStore.addLineItem(match.title)
  }
})

module.exports = Bom
