/** @jsx React.DOM */

var React = require('react')
var BS = require('react-bootstrap')
var _ = require('lodash')
var formatNumber = require('format-number')()
var constants = require('../constants')

var MpnSuggest = require('./MpnSuggest')
var DropdownButton = BS.DropdownButton;
var MenuItem = BS.MenuItem;

//
// BomRow
//
var BomRow = React.createClass({

  render: function() {
    var bom = this.props.bom
    var row = this.props.row
    var quantity = row.quantity
    var sellers = this.props.sellers
    var part = row.part

    var offerCells = sellers.map(function(seller) {
      if (!row.sellerOffer || !row.sellerOffer[seller]) return <td />
      var offer = row.sellerOffer[seller]
      return <BomOfferCell bom={bom} row={row} offer={offer}/>
    })

    return (
      <tr>
        <td>
          <BomRowInput row={row} />
        </td>
        <td className="borderLeft borderRight">
          <BomRowPartInfo row={row} />
        </td>

        {offerCells}

        <td className="borderLeft">
          <BestOfferInfo winner={row.winner} />
        </td>
        <td className="text-right">
          {this.currency(row.price)}
        </td>
        <td className="text-right">
          {this.currency(row.extendedTotal)}
        </td>
        <td className="text-right">
          {this.currency(row.grandTotal)}
        </td>

      </tr>
    )
  },

  currency: function (num) {
    if (!num) return <span>-</span>
    var bom = this.props.bom
    return <div>
      <span className="text-muted">{bom.currency} </span>
      <span>{num.toFixed(3)}</span>
    </div>
  }
})




//
// BomRowInput
//
var BomRowInput = React.createClass({

  updateQuantity: function (event) {
    var id = this.props.row.id
    var qty = event.target.value
    BomStore.setLineQty(id, qty)
  },

  updateQuery: function (m) {
    var id = this.props.row.id
    var query = m.title
    BomStore.setLineQuery(id, query)
  },

  deleteRow: function () {
    BomStore.deleteRow(this.props.row.id)
  },

  render: function () {
    var row = this.props.row
    var query = row.query
    var quantity = row.quantity
    return <div>
      <MpnSuggest searchTerm={query} onChange={this.updateQuery} />
      <input type="number" value={quantity} onChange={this.updateQuantity}/>
      <button onClick={this.deleteRow}>X</button>
    </div>
  }
})

// BomRowPartInfo
var BomRowPartInfo = React.createClass({

  setUid: function (uid) {
    var id = this.props.row.id
    BomStore.setLineUid(id, uid)
  },

  render: function() {
    var row = this.props.row
    var part = row.part

    if (row.state === constants.LOADING_STATE) {
      return <div>Loading...</div>
    }

    if (!part) {
      return <div>Not Found</div>
    }


    var numMatches = _.keys(row.matches).length
    var matchSelector = null
    if (numMatches > 1) {
      matchSelector = <DropdownButton
        bsSize="xsmall"
        title={numMatches}
        onSelect={this.setUid}>
          {_.map(row.matches, function(match, wut) {
            var txt = [match.brand, match.mpn].join(' ')
            return (
              <MenuItem
                className="matchSelectorMenuItem clearfix"
                key={match.uid}
                value={match.uid}>
                  <AvailBadge number={match.avail.stocking} />
                  <b>{txt}</b>
              </MenuItem>
            )
          })}
        </DropdownButton>
    }


    return (
      <div>
        <div className="pull-right">
          <AvailBadge number={part.avail.stocking} />
        </div>
        <div className="pull-right">
          {matchSelector}
        </div>
        <a href={part.url} target="_blank">
          <span>{part.brand}</span>
          <br />
          <span>{part.mpn}</span>
        </a>
      </div>
    )
  }
})

var AvailBadge = React.createClass({

  propTypes: {
    number: React.PropTypes.number
  },

  render: function() {
    var number = this.props.number

    // availabilityIndicator color
    var availStyle
    if (number > 2) {
      availStyle = {'background-color': '#00D580' }
    } else if (number > 0) {
      availStyle = {'background-color': '#FFC263' }
    } else {
      availStyle = {'background-color': '#FF6066' }
    }

    return this.transferPropsTo(
      <div className="availBadge" style={availStyle}>
        {number}
      </div>
    )
  }
})


//
// BomOfferCell - offer cell
//
var BomOfferCell = React.createClass({

  render: function () {
    var bom = this.props.bom
    var row = this.props.row
    var offer = this.props.offer
    var quantity = row.quantity

    if (!offer) {
      return <td />
    }

    var stock = formatNumber(offer.stock)

    var style = {}
    if (row.winner && row.winner.url === offer.url) {
      style['background-color'] = 'lightyellow'
    }

    var priceDiv = <div>-</div>
    if (offer.price) {
      priceDiv = <div>
        <span className="text-muted">{bom.currency}</span>
        {" "}
        {offer.price}
      </div>
    }

    return (
      <td className="text-right" style={style}>
        {priceDiv}
        <div className="text-muted small">stock: {stock}</div>
        <div className="text-muted small">moq: {offer.moq}</div>
      </td>
    )
  }
})


var BestOfferInfo = React.createClass({
  render: function () {
    var winner = this.props.winner
    if (!winner) return null
    return (
      <div>
        {winner.seller}<br/>
        {winner.sku}
      </div>
    )
  }
})


module.exports = BomRow
