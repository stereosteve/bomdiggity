/** @jsx React.DOM */

var React = require('react')
var BS = require('react-bootstrap')
var _ = require('lodash')
var BomStore = require('../bom_store')


var SellerSelectorModal = React.createClass({
  render: function() {
    return this.transferPropsTo(
        <BS.Modal title="Select Sellers" animation={true}>
          <div className="modal-body">
            <SellerSelector bom={this.props.bom} />
          </div>
          <div className="modal-footer">
            <BS.Button onClick={this.props.onRequestHide}>Close</BS.Button>
          </div>
        </BS.Modal>
      );
  }
});


var SellerSelector = React.createClass({

  render: function () {
    var bom = this.props.bom
    var sellers = _.keys(bom.sellerData)
    return <table className="table table-striped">
      <thead>
        <tr>
          <th>Seller Name</th>
          <th>Listed</th>
          <th>Stocked</th>
          <th>Authorized</th>
        </tr>
      </thead>
      <tbody>
        {sellers.map(function (seller) {
          return <SellerCheckbox key={seller} bom={bom} seller={seller} />
        })}
      </tbody>
    </table>
  }
})

var SellerCheckbox = React.createClass({

  toggleChecked: function (event) {
    BomStore.toggleSeller(this.props.seller)
  },

  render: function () {
    var bom = this.props.bom
    var seller = this.props.seller
    var sellerInfo = bom.sellerData[seller]
    var checked = bom.settings.sellerSelected[seller]
    return <tr>
      <td>
        <label>
          <input type="checkbox" checked={checked} onChange={this.toggleChecked}/> {seller}
        </label>
      </td>
      <td>{sellerInfo.rowsListed}</td>
      <td>{sellerInfo.rowsStocked}</td>
      <td>{sellerInfo.rowsAuthorized}</td>
    </tr>
  }
})



module.exports = SellerSelectorModal
