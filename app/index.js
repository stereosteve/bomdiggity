/** @jsx React.DOM */

var React = require('react')
window.React = React
var Bom = require('./components/Bom')

React.renderComponent(<Bom />, document.getElementById('AppContainer'))

var BomStore = require('./bom_store')
BomStore.load()


// debug
window.BomStore = BomStore
