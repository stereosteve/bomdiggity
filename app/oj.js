var _ = require('lodash')

exports.offerPriceAtQuantity = function(offer, currency, quantity) {
  if (offer.moq && quantity < offer.moq) return;
  var priceTuples = offer.prices[currency]
  if (!priceTuples) return
  var i, tuple, bestPrice
  for (i=0; i<priceTuples.length; i++) {
    tuple = priceTuples[i]
    if (tuple[0] > quantity) break
    bestPrice = tuple[1]
  }
  if (bestPrice) return Number(bestPrice)
}

exports.partAvailAtQuantity = function (part, currency, quantity) {
  var result = {
    listing: 0,
    stocking: 0
  }
  _.each(part.offers, function(o) {
    if (!o.prices[currency]) return;

    result.listing++

    if (o.in_stock_quantity > quantity)
      result.stocking++
  })
  return result;
}
